


INSTALLATION
------------
Выполнить команду
```
git clone https://Eugeny-Ivanov@bitbucket.org/Eugeny-Ivanov/web-stroy.git .
composer install
```
Настроить соединение с БД в файле /config/db.php выполнить коменду
```
php yii migrate
```
Настроить document_root сервера на каталог /web
```
root /var/www/example/web;
```
```
GET /api/v1/category: получение постранично списка всех категорий;
POST /api/v1/category/create: создание новой категории;
GET /api/v1/category/111: получение информации по конкретной категории с id равным 111;
PATCH /api/v1/category/123 и PUT /api/v1/category/123: изменение информации по категории с id равным 111;
```
метод delete не раелизован, не ясно что делать с вложенными подкатегориями