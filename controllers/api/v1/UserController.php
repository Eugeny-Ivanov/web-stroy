<?php

namespace app\controllers\api\v1;
use yii;
use app\models\user\LoginForm;
use app\models\user\User;
use yii\rest\ActiveController;
use app\models\user\SignupForm;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use \yii\web\ForbiddenHttpException;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\user\User';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
                'signup'
            ],
        ];

        $access = [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup', 'login', 'index', 'view'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except'=> ['options']
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'signup' => ['post'],
                ],
            ],
        ];
        $behaviors = array_merge($behaviors, $access);
        return $behaviors;
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            $user = $model->login();
        } else {
            $user = null;
        }
        /** @var Jwt $jwt */
        $jwt = \Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();

        // Previous implementation
        /*
        $token = $jwt->getBuilder()
            ->setIssuer('http://example.com')// Configures the issuer (iss claim)
            ->setAudience('http://example.org')// Configures the audience (aud claim)
            ->setId('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
            ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
            ->setExpiration(time() + 3600)// Configures the expiration time of the token (exp claim)
            ->set('uid', 100)// Configures a new claim, called "uid"
            ->sign($signer, $jwt->key)// creates a signature using [[Jwt::$key]]
            ->getToken(); // Retrieves the generated token
        */

        // Adoption for lcobucci/jwt ^4.0 version
        $token = $jwt->getBuilder()
            ->issuedBy('http://example.com')// Configures the issuer (iss claim)
            ->permittedFor('http://example.org')// Configures the audience (aud claim)
            ->identifiedBy('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
            ->issuedAt($time)// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $user->id)// Configures a new claim, called "uid"
            ->getToken($signer, $key); // Retrieves the generated token

        return $this->asJson([
            'user'=>$user,
            'token' => (string)$token,
        ]);
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        //var_dump(\Yii::$app->request->post()); die();
        if ($model->load(\Yii::$app->request->post(),'') && $model->signup()) {
            return ['message'=>'User registered', 'code'=>200];
        }
        return  $model;
    }

    /**
     * @return array|false|int[]|string[]
     */
    public function fields()
    {
        $fields = [
            'id',
            'username',
            'email',
        ];

        return $fields;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if(in_array($action, ['view', 'update', 'delete']) && $model->id != Yii::$app->user->identity->getId()) {
            throw new \yii\web\ForbiddenHttpException(sprintf('You are not allowed.', $action));
        }
    }
}
