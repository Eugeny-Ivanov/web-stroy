<?php


namespace app\controllers\api\v1;

use app\models\product\ProductSets;
use yii;
use app\models\product\Product;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
class ProductController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\product\Product';
}