<?php

namespace app\models\product;

use Yii;

/**
 * This is the model class for table "product_offers_list".
 *
 * @property int $id
 * @property string|null $label
 * @property float|null $price
 *
 * @property ProductOffers[] $productOffers
 */
class ProductOffersList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_offers_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price'], 'number'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'price' => 'Price',
        ];
    }

    /**
     * Gets query for [[ProductOffers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductOffers()
    {
        return $this->hasMany(ProductOffers::className(), ['offer_id' => 'id']);
    }
}
