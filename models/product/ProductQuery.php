<?php

namespace app\models\product;

/**
 * This is the ActiveQuery class for [[Proguct]].
 *
 * @see Proguct
 */
class ProductQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Proguct[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Proguct|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
