--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: hostwe4g; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA hostwe4g;


ALTER SCHEMA hostwe4g OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    tree integer,
    lft integer NOT NULL,
    rgt integer NOT NULL,
    depth integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at integer,
    updated_at integer,
    status smallint,
    created_by integer,
    updated_by integer
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time integer
);


ALTER TABLE public.migration OWNER TO postgres;

--
-- Name: proguct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proguct (
    id integer NOT NULL,
    category_id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    type smallint,
    created_at integer,
    updated_at integer,
    status smallint,
    created_by integer,
    updated_by integer
);


ALTER TABLE public.proguct OWNER TO postgres;

--
-- Name: proguct_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proguct_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proguct_id_seq OWNER TO postgres;

--
-- Name: proguct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proguct_id_seq OWNED BY public.proguct.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(255),
    auth_key character varying(32) NOT NULL,
    password_hash character varying(255) NOT NULL,
    password_reset_token character varying(255),
    email character varying(255) NOT NULL,
    status smallint DEFAULT 10 NOT NULL,
    created_at integer NOT NULL,
    updated_at integer NOT NULL,
    verification_token character varying(255)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: proguct id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proguct ALTER COLUMN id SET DEFAULT nextval('public.proguct_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, tree, lft, rgt, depth, name, created_at, updated_at, status, created_by, updated_by) FROM stdin;
1	1	1	4	0	Probuct1	\N	\N	\N	\N	\N
4	1	2	3	1	Probuct1-2	\N	\N	\N	\N	\N
\.


--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration (version, apply_time) FROM stdin;
m000000_000000_base	1616398247
m210322_061821_create_category_table	1616398251
m210322_062108_create_proguct_table	1616398251
m210322_124647_add_fields_category_table	1616424274
m210322_131406_add_fields_product_table	1616424274
\.


--
-- Data for Name: proguct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proguct (id, category_id, title, description, type, created_at, updated_at, status, created_by, updated_by) FROM stdin;
1	1	Product1	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	\N	\N	\N	\N	\N	\N
2	4	Product2	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, username, auth_key, password_hash, password_reset_token, email, status, created_at, updated_at, verification_token) FROM stdin;
4	admin	tbaxtHK1hJaMGEg0SMF8R8EftL8BP1O-	$2y$13$IN2kKQ5vx5Z.5JXo4L0wye5mUmh5Wcs7kvCse4GrgW/tGBOtDkL8S	\N	aaa@bbb.ru	10	1616425482	1616425846	KOAjMsmyA_AIZwGQal0bf6g9DsApG7-o_1616425482
5	demo	nGhh5Xqg1nXfFEBvcduC6kkfz_jMpDLj	$2y$13$lQvrdPWVO8bjUL/Wsm3SI.swV18KYk49Oulwh0BKK95IXVf81GKCu	\N	aaa@bbb1.ru	10	1616427120	1616427170	NN7Hta_TSbqHNLL9hcbBMkpkUPVPFnL2_1616427120
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 4, true);


--
-- Name: proguct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proguct_id_seq', 2, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 5, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: migration migration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: proguct proguct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proguct
    ADD CONSTRAINT proguct_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_password_reset_token_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_password_reset_token_key UNIQUE (password_reset_token);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: idx-category-category_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-category-category_id" ON public.proguct USING btree (category_id);


--
-- Name: idx-proguct-created_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-proguct-created_by" ON public.proguct USING btree (created_by);


--
-- Name: idx-proguct-updated_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-proguct-updated_by" ON public.proguct USING btree (updated_by);


--
-- Name: idx-user-created_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-user-created_by" ON public.category USING btree (created_by);


--
-- Name: idx-user-updated_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-user-updated_by" ON public.category USING btree (updated_by);


--
-- Name: lft; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lft ON public.category USING btree (tree, lft, rgt);


--
-- Name: rgt; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rgt ON public.category USING btree (tree, rgt);


--
-- Name: proguct fk-category-category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proguct
    ADD CONSTRAINT "fk-category-category_id" FOREIGN KEY (category_id) REFERENCES public.category(id) ON DELETE CASCADE;


--
-- Name: proguct fk-proguct-created_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proguct
    ADD CONSTRAINT "fk-proguct-created_by" FOREIGN KEY (created_by) REFERENCES public."user"(id);


--
-- Name: proguct fk-proguct-updated_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proguct
    ADD CONSTRAINT "fk-proguct-updated_by" FOREIGN KEY (updated_by) REFERENCES public."user"(id);


--
-- Name: category fk-user-created_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "fk-user-created_by" FOREIGN KEY (created_by) REFERENCES public."user"(id);


--
-- Name: category fk-user-updated_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "fk-user-updated_by" FOREIGN KEY (updated_by) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

