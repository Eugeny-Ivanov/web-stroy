--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    tree integer,
    lft integer NOT NULL,
    rgt integer NOT NULL,
    depth integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at integer,
    updated_at integer,
    status smallint,
    created_by integer,
    updated_by integer
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time integer
);


ALTER TABLE public.migration OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    id integer NOT NULL,
    category_id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    type smallint,
    created_at integer,
    updated_at integer,
    status smallint,
    created_by integer,
    updated_by integer
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: product_offers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_offers (
    id integer NOT NULL,
    product_id integer,
    offer_id integer,
    price numeric(19,4)
);


ALTER TABLE public.product_offers OWNER TO postgres;

--
-- Name: product_offers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_offers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_offers_id_seq OWNER TO postgres;

--
-- Name: product_offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_offers_id_seq OWNED BY public.product_offers.id;


--
-- Name: product_offers_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_offers_list (
    id integer NOT NULL,
    label character varying(255),
    price numeric(19,4)
);


ALTER TABLE public.product_offers_list OWNER TO postgres;

--
-- Name: product_offers_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_offers_list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_offers_list_id_seq OWNER TO postgres;

--
-- Name: product_offers_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_offers_list_id_seq OWNED BY public.product_offers_list.id;


--
-- Name: product_sets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_sets (
    id integer NOT NULL,
    product_id integer,
    set_product_id integer
);


ALTER TABLE public.product_sets OWNER TO postgres;

--
-- Name: product_sets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_sets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_sets_id_seq OWNER TO postgres;

--
-- Name: product_sets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_sets_id_seq OWNED BY public.product_sets.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(255),
    auth_key character varying(32) NOT NULL,
    password_hash character varying(255) NOT NULL,
    password_reset_token character varying(255),
    email character varying(255) NOT NULL,
    status smallint DEFAULT 10 NOT NULL,
    created_at integer NOT NULL,
    updated_at integer NOT NULL,
    verification_token character varying(255)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: product_offers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers ALTER COLUMN id SET DEFAULT nextval('public.product_offers_id_seq'::regclass);


--
-- Name: product_offers_list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers_list ALTER COLUMN id SET DEFAULT nextval('public.product_offers_list_id_seq'::regclass);


--
-- Name: product_sets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets ALTER COLUMN id SET DEFAULT nextval('public.product_sets_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, tree, lft, rgt, depth, name, created_at, updated_at, status, created_by, updated_by) FROM stdin;
2	1	2	5	1	Первая	1616489650	1616489650	1	\N	\N
5	1	3	4	2	Первая-1-2	1616489722	1616489722	1	\N	\N
3	1	6	9	1	Первая-1-1	1616489667	1616489667	1	\N	\N
4	1	10	11	1	Первая-1-1	1616489674	1616489674	1	\N	\N
1	1	1	12	0	Первая	1616489614	1616489614	1	\N	\N
6	1	7	8	2	Первая-1-3	1616490578	1616490578	1	\N	\N
\.


--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration (version, apply_time) FROM stdin;
m000000_000000_base	1616489153
m210322_061821_create_category_table	1616489170
m210322_062108_create_product_table	1616489242
m210322_124647_add_fields_category_table	1616489242
m210322_131406_add_fields_product_table	1616489242
m210323_050121_create_product_sets_table	1616493089
m210323_075422_create_product_offers_table	1616493412
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product (id, category_id, title, description, type, created_at, updated_at, status, created_by, updated_by) FROM stdin;
1	1	Набор2	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616490814	1616490814	1	1	\N
2	1	Набор3	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616490841	1616490841	1	1	\N
3	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616490881	1616490881	1	1	\N
4	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616491798	1616491798	1	\N	\N
5	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616492407	1616492407	1	\N	\N
6	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616493800	1616493800	1	\N	\N
7	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616494132	1616494132	1	\N	\N
11	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616494541	1616494541	1	\N	\N
12	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616494575	1616494575	1	\N	\N
13	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616494690	1616494690	1	\N	\N
14	1	Набор4	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	1	1616494988	1616494988	1	\N	\N
\.


--
-- Data for Name: product_offers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_offers (id, product_id, offer_id, price) FROM stdin;
1	7	\N	\N
2	7	\N	\N
3	11	1	\N
4	11	2	\N
5	12	1	\N
6	12	2	\N
7	13	1	\N
8	13	2	\N
9	14	1	\N
10	14	3	\N
\.


--
-- Data for Name: product_offers_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_offers_list (id, label, price) FROM stdin;
1	150*300	1300.3690
2	150*200	1300.3690
3	150*400	1300.0000
\.


--
-- Data for Name: product_sets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_sets (id, product_id, set_product_id) FROM stdin;
1	6	1
2	6	2
3	6	1
4	7	1
5	7	2
6	7	1
16	11	1
17	11	2
18	11	1
19	12	1
20	12	2
21	12	1
22	13	1
23	13	2
24	13	1
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, username, auth_key, password_hash, password_reset_token, email, status, created_at, updated_at, verification_token) FROM stdin;
1	admin	Gsf5BXju-2fcm3eKC9jOiAjCkojzr2bO	$2y$13$rdiHpgyZMMAPIjkrWWO3XeeJ2tdDT.mzIkeWe.i5jpvQZhwZNH39m	\N	aaa@bbb.ru	10	1616489317	1616489369	5Tf4Bc77Shnz9gtKlhVxutyIZiAmsFeC_1616489317
2	demo	JKqMAPcXUgqtitIZsgZ9eo5L4DDzFAz8	$2y$13$nMd7aTTBtgDnirqvw9mdYu3tk86XbhU16baKXtsGJ2jCSnEDXtk5i	\N	aaa@bbb1.ru	10	1616489336	1616489382	mVf6CyM4oF32GsvCWkDouQeWYY1r0a9T_1616489336
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 6, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_id_seq', 14, true);


--
-- Name: product_offers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_offers_id_seq', 10, true);


--
-- Name: product_offers_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_offers_list_id_seq', 3, true);


--
-- Name: product_sets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_sets_id_seq', 24, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 2, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: migration migration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: product_offers_list product_offers_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers_list
    ADD CONSTRAINT product_offers_list_pkey PRIMARY KEY (id);


--
-- Name: product_offers product_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers
    ADD CONSTRAINT product_offers_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_sets product_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets
    ADD CONSTRAINT product_sets_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_password_reset_token_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_password_reset_token_key UNIQUE (password_reset_token);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: idx-category-category_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-category-category_id" ON public.product USING btree (category_id);


--
-- Name: idx-offers-offer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-offers-offer_id" ON public.product_offers USING btree (offer_id);


--
-- Name: idx-offers-product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-offers-product_id" ON public.product_offers USING btree (product_id);


--
-- Name: idx-product-created_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-product-created_by" ON public.product USING btree (created_by);


--
-- Name: idx-product-updated_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-product-updated_by" ON public.product USING btree (updated_by);


--
-- Name: idx-set_product-product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-set_product-product_id" ON public.product_sets USING btree (product_id);


--
-- Name: idx-set_product-set_product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-set_product-set_product_id" ON public.product_sets USING btree (set_product_id);


--
-- Name: idx-user-created_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-user-created_by" ON public.category USING btree (created_by);


--
-- Name: idx-user-updated_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-user-updated_by" ON public.category USING btree (updated_by);


--
-- Name: lft; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lft ON public.category USING btree (tree, lft, rgt);


--
-- Name: rgt; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rgt ON public.category USING btree (tree, rgt);


--
-- Name: product fk-category-category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "fk-category-category_id" FOREIGN KEY (category_id) REFERENCES public.category(id) ON DELETE CASCADE;


--
-- Name: product_offers fk-offers-offer_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers
    ADD CONSTRAINT "fk-offers-offer_id" FOREIGN KEY (offer_id) REFERENCES public.product_offers_list(id) ON DELETE CASCADE;


--
-- Name: product_offers fk-offers-product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers
    ADD CONSTRAINT "fk-offers-product_id" FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product fk-product-created_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "fk-product-created_by" FOREIGN KEY (created_by) REFERENCES public."user"(id);


--
-- Name: product fk-product-updated_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "fk-product-updated_by" FOREIGN KEY (updated_by) REFERENCES public."user"(id);


--
-- Name: product_sets fk-set_product-product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets
    ADD CONSTRAINT "fk-set_product-product_id" FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product_sets fk-set_product-set_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets
    ADD CONSTRAINT "fk-set_product-set_product_id" FOREIGN KEY (set_product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: category fk-user-created_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "fk-user-created_by" FOREIGN KEY (created_by) REFERENCES public."user"(id);


--
-- Name: category fk-user-updated_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "fk-user-updated_by" FOREIGN KEY (updated_by) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

