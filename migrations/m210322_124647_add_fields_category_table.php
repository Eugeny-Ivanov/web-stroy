<?php

use yii\db\Migration;

/**
 * Class m210322_124647_add_fields_category_table
 */
class m210322_124647_add_fields_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'verification_token' => $this->string(),
        ]);
        $this->addColumn('{{%category}}', 'created_at', $this->integer());
        $this->addColumn('{{%category}}', 'updated_at', $this->integer());
        $this->addColumn('{{%category}}', 'status', $this->tinyInteger(1));
        $this->addColumn('{{%category}}', 'created_by', $this->integer());
        $this->addColumn('{{%category}}', 'updated_by', $this->integer());
        $this->addColumn('{{%product}}', 'type', $this->tinyInteger(1));
        $this->createIndex(
            'idx-user-created_by',
            '{{%category}}',
            'created_by'
        );
        $this->createIndex(
            'idx-user-updated_by',
            '{{%category}}',
            'updated_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-created_by',
            '{{%category}}',
            'created_by',
            '{{%user}}',
            'id'
        );
        $this->addForeignKey(
            'fk-user-updated_by',
            '{{%category}}',
            'updated_by',
            '{{%user}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'created_at');
        $this->dropColumn('{{%category}}', 'updated_at');
        $this->dropColumn('{{%category}}', 'status');
        $this->dropColumn('{{%category}}', 'created_by');
        $this->dropColumn('{{%category}}', 'updated_by');
        $this->dropColumn('{{%product}}', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210322_124647_add_fields_category_table cannot be reverted.\n";

        return false;
    }
    */
}
