<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `{{%category}}`.
 */
class m210322_061821_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id'    => Schema::TYPE_PK,
            'tree'  => Schema::TYPE_INTEGER . ' NULL',
            'lft'   => Schema::TYPE_INTEGER . ' NOT NULL',
            'rgt'   => Schema::TYPE_INTEGER . ' NOT NULL',
            'depth' => Schema::TYPE_INTEGER . ' NOT NULL', // not unsigned!
            'name'  => Schema::TYPE_STRING . ' NOT NULL', // example field
        ]);
        $this->createIndex('lft', '{{%category}}', ['tree', 'lft', 'rgt']);
        $this->createIndex('rgt', '{{%category}}', ['tree', 'rgt']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}
