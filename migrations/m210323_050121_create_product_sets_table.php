<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_sets}}`.
 */
class m210323_050121_create_product_sets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_sets}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'set_product_id' => $this->integer(),
        ]);
        $this->createIndex(
            'idx-set_product-product_id',
            '{{%product_sets}}',
            'product_id'
        );
        $this->createIndex(
            'idx-set_product-set_product_id',
            '{{%product_sets}}',
            'set_product_id'
        );
        $this->addForeignKey(
            'fk-set_product-product_id',
            '{{%product_sets}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-set_product-set_product_id',
            '{{%product_sets}}',
            'set_product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_sets}}');
    }
}
