<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_offers}}`.
 */
class m210323_075422_create_product_offers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_offers_list}}', [
            'id' => $this->primaryKey(),
            'label'=>$this->string(),
            'price' => $this->money(),
        ]);

        $this->createTable('{{%product_offers}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'offer_id' => $this->integer(),
            'price' => $this->money(),
        ]);
        $this->createIndex(
            'idx-offers-product_id',
            '{{%product_offers}}',
            'product_id'
        );
        $this->createIndex(
            'idx-offers-offer_id',
            '{{%product_offers}}',
            'offer_id'
        );
        $this->addForeignKey(
            'fk-offers-product_id',
            '{{%product_offers}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-offers-offer_id',
            '{{%product_offers}}',
            'offer_id',
            '{{%product_offers_list}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_offers}}');
    }
}
